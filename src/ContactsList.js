import React, { Component } from 'react';
import axios from 'axios';

const ContactItem = ({ contact }) => (
  <li>Name: {contact.name}, Subject: {contact.message}, Message: {contact.message}</li>
)

class ContactsList extends Component {
  state = {
    contacts: []
  }

  componentDidMount() {
    this.fetchInterval = setInterval(
      () => {
        axios.get('http://localhost:3001/contacts').then(
          response => this.setState({ contacts: response.data })
        ).catch(
          error => console.error(error)
        )
      }, 2000
    )
  }

  componentWillUnmount() {
    clearInterval(this.fetchInterval);
  }

  render() {
    return (
      <div>
        <h2>Contact List</h2>
        <ul>
          {
            this.state.contacts.map(c => <ContactItem key={c.id} contact={c} />)
          }
        </ul>
      </div>
    )
  }
}

export default ContactsList;