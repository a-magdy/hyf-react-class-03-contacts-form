import React, { Component } from 'react'
import axios from 'axios';

class ContactsForm extends Component {
  state = {
    name: '',
    subject: '',
    message: ''
  }

  handleInputChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value
    })
  }

  handleSubmit = (e) => {
    e.preventDefault();

    // axios.post('http://localhost:3001/contacts', this.state);
    axios.post('http://localhost:3001/contacts', {
      name: this.state.name,
      subject: this.state.subject,
      message: this.state.message
    }).then(
      response => {
        // alert('message saved successfully')
        this.setState({
          name: '',
          subject: '',
          message: ''
        })
      }
    ).catch(
      error => console.error(error)
    )
  }

  render() {
    return (
      <form onSubmit={this.handleSubmit}>

        <label>
          Name
          <input type="text" name="name" value={this.state.name} onChange={this.handleInputChange} />
        </label>

        <label>
          Subject
          <input type="text" name="subject" value={this.state.subject} onChange={this.handleInputChange} />
        </label>

        <label>
          Message 
          <input type="text" name="message" value={this.state.message} onChange={this.handleInputChange} />
        </label>

        <input type="submit" value="Submit" />

        <p>{this.state.name}</p>
        <p>{this.state.subject}</p>
        <p>{this.state.message}</p>
      </form>
    )
  }
}

export default ContactsForm;